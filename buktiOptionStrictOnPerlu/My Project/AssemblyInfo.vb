﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("buktiOptionStrictOnPerlu")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("PT Ultrajaya Milk Industry Tbk")> 
<Assembly: AssemblyProduct("buktiOptionStrictOnPerlu")> 
<Assembly: AssemblyCopyright("Copyright © PT Ultrajaya Milk Industry Tbk 2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f3b5d456-cb2e-4e51-96a0-25095b5ce0d9")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
