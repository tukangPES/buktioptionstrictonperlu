﻿Option Strict On

Module Module1

    Sub Main()

        ' ada contoh dalam praktek sehari2

        Dim aParkir As String
        Dim aBiayaBelanja As String
        Dim aTotalPengeluaran As Double

        aBiayaBelanja = "860.45"
        aParkir = "500"
        aTotalPengeluaran = aBiayaBelanja + aParkir
        Console.WriteLine(aTotalPengeluaran)

        'berapakah nilai dari Total Pengeluaran? jika kamu expect 1360.45 maka km salah
        '
        '----------------
        'khan seharusnya hasil yg diperoleh adalah 1360.45 (dari perhitungan simpel 860.45 + 500)
        'tetapi ini malahan 860.455 ( dari perhitungan string simpel 860.45 concatenate 500 = 860.45500)

        'nah dengan option strict on maka akan ada warning ttg ini,
        'sehngga kita bisa aware sebelum publish
        Console.ReadKey()

    End Sub

End Module
